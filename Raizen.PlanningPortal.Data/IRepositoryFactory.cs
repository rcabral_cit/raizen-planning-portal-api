﻿using System;

namespace Raizen.PlanningPortal.Data
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
    }
}