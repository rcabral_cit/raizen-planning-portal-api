﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;


namespace Raizen.PlanningPortal.Data
{
	public class PlanningPortalContextFactory : IDesignTimeDbContextFactory<PlanningPortalContext>
    {
		public PlanningPortalContext CreateDbContext(string[] args)
        {
            // TODO: Refactor Hardcoded Connection String
			var optionsBuilder = new DbContextOptionsBuilder<PlanningPortalContext>()
				.UseSqlServer("Server=localhost;Database=PlanningPortal;User=sa;Password=Suporte_123456;Integrated Security=false;");

			return new PlanningPortalContext(optionsBuilder.Options);
        }
    }
}
