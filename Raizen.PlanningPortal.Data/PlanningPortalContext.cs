﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Raizen.PlanningPortal.Data
{
	public class PlanningPortalContext : DbContext
    {
		public PlanningPortalContext(DbContextOptions<PlanningPortalContext> options)
            : base(options)
        {

        }

        // Include here yours DBSet
        // Example: public DbSet<TodoItem> TodoItems { get; set; }
    }
}