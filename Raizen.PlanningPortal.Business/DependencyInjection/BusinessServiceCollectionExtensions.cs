﻿using Microsoft.Extensions.DependencyInjection;

namespace Raizen.PlanningPortal.Business.DependencyInjection
{
    public static class BusinessServiceCollectionExtensions
    {
        public static IServiceCollection AddBussinessDependencyInjection(this IServiceCollection services)
        {
            // Include here yours services to DependencyInjection
            // Example: services.AddTransient<ITodoItemService, TodoItemService>();

            return services;
        }
    }
}