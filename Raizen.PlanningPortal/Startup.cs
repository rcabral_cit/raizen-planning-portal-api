﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Raizen.PlanningPortal.Business.DependencyInjection;
using Raizen.PlanningPortal.Data;
using Raizen.PlanningPortal.Data.DependencyInjection;

namespace Raizen.PlanningPortal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<PlanningPortalContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
                    .AddUnitOfWork<PlanningPortalContext>();
            
            services.AddBussinessDependencyInjection();
            
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            UpdateDatabase(app);
            
            app.UseMvc();
        }

		private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<PlanningPortalContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
